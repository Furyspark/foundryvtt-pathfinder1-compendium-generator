import Importable from "./importable.js";
import { randomID } from "../lib.js";
import { domains } from "../data/domains.js";

interface ItemDescriptionTemplateData {
  description: {
    value: string,
    chat: string,
    unidentified: string,
  },
  source: string,
}

interface ActivatedEffectTemplateData {
  activation: {
    cost: number,
    type: string,
  },
  duration: {
    value: number,
    units: string,
  },
  target: {
    value: string,
  },
  range: {
    value: number,
    units: string,
  },
  uses: {
    value: number,
    max: number,
    per: string,
  },
}

interface ActionTemplateData {
  measureTemplate: {
    type: string,
    size: number,
  },
  actionType: string,
  attackBonus: string,
  critConfirmBonus: string,
  damage: {
    parts: string[],
  },
  attackParts: string[],
  formula: string,
  ability: {
    attack: string,
    damage: string,
    damageMult: number,
    critRange: number,
    critMult: number,
  },
  save: {
    dc: string,
    description: string,
  },
  effectNotes: string,
  attackNotes: string,
}

interface StringNumberPair {
  0: string,
  1: number,
}

interface SpellData {
  _id: string,
  name: string,
  type: string,
  img: string,
  data: SpellTemplateData,
}

interface SpellTemplateData extends ActivatedEffectTemplateData, ActionTemplateData, ItemDescriptionTemplateData {
  school: string,
  subschool: string,
  savingThrow: string,
  sr: boolean,
  learnedAt: {
    class: StringNumberPair[],
    domain: StringNumberPair[],
    subDomain: StringNumberPair[],
    elementalSchool: StringNumberPair[],
    bloodline: StringNumberPair[],
  },
  level: number
  clOffset: number,
  slOffset: number,
  types: string,
  spellDuration: string,
  components: {
    value: string,
    verbal: boolean,
    somatic: boolean,
    material: boolean,
    focus: boolean,
    divineFocus: number,
  },
  materials: {
    value: string,
    focus: string,
  },
  spellbook: string,
  preparation: {
    mode: string,
    preparedAmount: number,
    maxAmount: number,
  },
  shortDescription: string,
  spellEffect: string,
  spellArea: string,
}

export default class Spell extends Importable {
  data: SpellData

  constructor(src: string[]) {
    super();

    this.data = {
      _id: randomID(16),
      name: src[0],
      type: "spell",
      img: "systems/pf1/icons/misc/magic-swirl.png",
      data: {
        school: this.parseSpellSchool(src),
        subschool: src[2],
        savingThrow: src[15],
        sr: src[16].startsWith("yes"),
        activation: {
          cost: 1,
          type: "",
        },
        duration: {
          value: null,
          units: "",
        },
        target: {
          value: src[11],
        },
        range: {
          value: null,
          units: "",
        },
        uses: {
          value: 0,
          max: 0,
          per: "",
        },
        learnedAt: {
          class: this.parseLevelRequirements("class", src),
          domain: this.parseLevelRequirements("domain", src),
          subDomain: this.parseLevelRequirements("subdomain", src),
          elementalSchool: [],
          bloodline: this.parseLevelRequirements("bloodline", src),
        },
        level: this.getLevel(src),
        clOffset: 0,
        slOffset: 0,
        types: this.parseSpellDescriptors(src),
        spellDuration: src[12],
        components: {
          value: "",
          verbal: false,
          somatic: false,
          material: false,
          focus: false,
          divineFocus: 0,
        },
        materials: {
          value: "",
          focus: "",
        },
        spellbook: "primary",
        preparation: {
          mode: "prepared",
          preparedAmount: 0,
          maxAmount: 0,
        },
        shortDescription: this.parseSpellDescription(src),
        spellEffect: src[10],
        spellArea: src[9],
        measureTemplate: {
          type: "",
          size: 0,
        },
        actionType: "",
        attackBonus: "",
        critConfirmBonus: "",
        damage: {
          parts: [],
        },
        attackParts: [],
        formula: "",
        ability: {
          attack: "",
          damage: "",
          damageMult: 1,
          critRange: 20,
          critMult: 2,
        },
        save: {
          dc: "0",
          description: "",
        },
        effectNotes: "",
        attackNotes: "",
        description: {
          value: "",
          chat: "",
          unidentified: "",
        },
        source: src[19],
      },
    };

    this._postProcess(src);
  }

  _postProcess(src: string[]) {
    this._postProcessComponents(src);
    this._postProcessActivation(src);
    this._postProcessAction(src);
  }

  parseSpellDescription(src: string[]): string {
    return src[18].replace(/"/g, '\\"');
  }

  parseSpellSchool(src: string[]): string {
    let school = src[1];
    switch (school.toLowerCase()) {
      case "conjuration":
        return "con";
      case "divination":
        return "div";
      case "enchantment":
        return "enc";
      case "evocation":
        return "evo";
      case "illusion":
        return "ill";
      case "necromancy":
        return "nec";
      case "transmutation":
        return "trs";
      case "universal":
        return "uni";
    }
    return "abj";
  }

  parseLevelRequirements(type: string, src: string[]): StringNumberPair[] {
    let result = [];

    if (type === "class") {
      if (src[26] !== "NULL") result.push(["sorcerer/wizard", parseInt(src[26])]);
      // if (src[27] !== "NULL") result.push(["wizard", parseInt(src[27])]);
      if (src[28] !== "NULL") result.push(["cleric/oracle", parseInt(src[28])]);
      if (src[29] !== "NULL") result.push(["druid", parseInt(src[29])]);
      if (src[30] !== "NULL") result.push(["ranger", parseInt(src[30])]);
      if (src[31] !== "NULL") result.push(["bard", parseInt(src[31])]);
      if (src[32] !== "NULL") result.push(["paladin", parseInt(src[32])]);
      if (src[33] !== "NULL") result.push(["alchemist", parseInt(src[33])]);
      if (src[34] !== "NULL") result.push(["summoner", parseInt(src[34])]);
      if (src[35] !== "NULL") result.push(["witch", parseInt(src[35])]);
      if (src[36] !== "NULL") result.push(["inquisitor", parseInt(src[36])]);
      // if (src[37] !== "NULL") result.push(["oracle", parseInt(src[37])]);
      if (src[38] !== "NULL") result.push(["antipaladin", parseInt(src[38])]);
      if (src[39] !== "NULL") result.push(["magus", parseInt(src[39])]);
      if (src[40] !== "NULL") result.push(["adept", parseInt(src[40])]);
      if (src[78] !== "NULL") result.push(["bloodrager", parseInt(src[78])]);
      if (src[79] !== "NULL") result.push(["shaman", parseInt(src[79])]);
      if (src[80] !== "NULL") result.push(["psychic", parseInt(src[80])]);
      if (src[81] !== "NULL") result.push(["medium", parseInt(src[81])]);
      if (src[82] !== "NULL") result.push(["mesmerist", parseInt(src[82])]);
      if (src[83] !== "NULL") result.push(["occultist", parseInt(src[83])]);
      if (src[84] !== "NULL") result.push(["spiritualist", parseInt(src[84])]);
      if (src[85] !== "NULL") result.push(["skald", parseInt(src[85])]);
      if (src[86] !== "NULL") result.push(["investigator", parseInt(src[86])]);
      if (src[87] !== "NULL") result.push(["hunter", parseInt(src[87])]);
      if (src[92] !== "NULL") result.push(["unchained Summoner", parseInt(src[92])]);
    }

    else if (type === "domain" || type === "subdomain") {
      if (src[43].length) {
        let domainDescriptor = src[43].split(/\s*,\s*/);
        for (let dom of domainDescriptor) {
          if (dom.match(/^([a-zA-Z]+)\s+\(([0-9])\)$/)) {
            let domain = RegExp.$1;
            let level = parseInt(RegExp.$2);
            if (type === "domain" && !domains.includes(domain)) continue;
            if (type === "subdomain" && domains.includes(domain)) continue;
            result.push([domain, level]);
          }
        }
      }
    }

    else if (type === "bloodline") {
      if (src[73].length) {
        let bloodlineDescriptor = src[73].split(/\s*,\s*/);
        for (let d of bloodlineDescriptor) {
          if (d.match(/^([a-zA-Z]+)\s+\(([0-9]+)\)$/)) {
            let bloodline = RegExp.$1;
            let level = parseInt(RegExp.$2);
            // Translate sorcerer level to spell level
            if (level > 1) level = Math.floor(level / 2);
            // Add to list
            result.push([bloodline, level]);
          }
        }
      }
    }

    return result;
  }

  getLevel(src: string[]): number {
    if (src[26] !== "NULL") return parseInt(src[26]);
    if (src[27] !== "NULL") return parseInt(src[27]);
    if (src[28] !== "NULL") return parseInt(src[28]);
    if (src[29] !== "NULL") return parseInt(src[29]);
    if (src[30] !== "NULL") return parseInt(src[30]);
    if (src[31] !== "NULL") return parseInt(src[31]);
    if (src[32] !== "NULL") return parseInt(src[32]);
    if (src[33] !== "NULL") return parseInt(src[33]);
    if (src[34] !== "NULL") return parseInt(src[34]);
    if (src[35] !== "NULL") return parseInt(src[35]);
    if (src[36] !== "NULL") return parseInt(src[36]);
    if (src[37] !== "NULL") return parseInt(src[37]);
    if (src[38] !== "NULL") return parseInt(src[38]);
    if (src[39] !== "NULL") return parseInt(src[39]);
    if (src[40] !== "NULL") return parseInt(src[40]);
    if (src[78] !== "NULL") return parseInt(src[78]);
    if (src[79] !== "NULL") return parseInt(src[79]);
    if (src[80] !== "NULL") return parseInt(src[80]);
    if (src[81] !== "NULL") return parseInt(src[81]);
    if (src[82] !== "NULL") return parseInt(src[82]);
    if (src[83] !== "NULL") return parseInt(src[83]);
    if (src[84] !== "NULL") return parseInt(src[84]);
    if (src[85] !== "NULL") return parseInt(src[85]);
    if (src[86] !== "NULL") return parseInt(src[86]);
    if (src[87] !== "NULL") return parseInt(src[87]);
    if (src[92] !== "NULL") return parseInt(src[92]);
    return 0;
  }

  parseSpellDescriptors(src: string[]) {
    let descriptors = src[3].split(/\s*[,;]\s*/);
    return descriptors.map(s => {
      if (s.endsWith("UM")) return s.substring(0, s.length - 2);
      return s;
    }).join(", ");
  }

  _postProcessComponents(src: string[]) {
    let data = this.data.data;
    let components = src[6];
    if (!components.length) return;

    // Split component string
    let re = /([,\(\)])/g,
      reObj = null,
      openBrackets = 0,
      cmps = [],
      lastSplitIndex = -1;
    while ((reObj = re.exec(components)) != null) {
      if (reObj[0] === ",") {
        if (openBrackets === 0) {
          cmps.push(components.substring(lastSplitIndex + 1, reObj.index).trim());
          lastSplitIndex = reObj.index;
        }
      }
      else if (reObj[0] === "(") openBrackets++;
      else if (reObj[0] === ")") openBrackets = Math.max(0, openBrackets - 1);
    }
    if (openBrackets === 0) {
      cmps.push(components.substring(lastSplitIndex + 1).trim());
    }

    // Add results
    for (let c of cmps) {
      if (c === "V") data.components.verbal = true;
      else if (c === "S") data.components.somatic = true;
      else if (c === "DF") data.components.divineFocus = 1;
      else if (c.match(/(M\/DF|DF\/M|M)\s+\(([\w\d\s,.\-_\+\|'";:]+)\)/)) {
        let type = RegExp.$1;
        let value = RegExp.$2;
        if (["M/DF", "DF/M"].includes(type)) data.components.divineFocus = 2;
        data.components.material = true;
        data.materials.value = value;
      }
      else if (c.match(/(F\/DF|DF\/F|F)\s+\(([\w\d\s,.\-_\+\|'";:]+)\)/)) {
        let type = RegExp.$1;
        let value = RegExp.$2;
        if (["F/DF", "DF/F"].includes(type)) data.components.divineFocus = 3;
        data.components.focus = true;
        data.materials.focus = value;
      }
      else if (!["M", "F"].includes(c) && c.match(/^(?:[a-zA-Z\s]+)$/)) {
        data.components.value = c;
      }
    }
  }

  _postProcessActivation(src: string[]) {
    let data = this.data.data;

    // Casting time
    {
      const ct = src[5];
      if (ct.match(/^(?:([0-9]+)\s+)?(minute|hour|standard|move|swift|immediate|free|round|full[\s-]?round)/i)) {
        data.activation.cost = parseInt(RegExp.$1) || 1;
        let type = RegExp.$2;
        if (["minute", "hour", "round", "standard", "move", "immediate", "swift", "free"].includes(type)) data.activation.type = type;
        else if (type.match(/^full[\s-]?round$/i)) data.activation.type = "full";
        else data.activation.type = "standard";
      }
    }
    // Spell range
    {
      const rng = src[8];
      if (rng.match(/^(close|medium|long|personal|touch)/)) {
        data.range.units = RegExp.$1;
      }
      else if (rng.match(/^([0-9]+)\s+(ft|feet|mi|mile)/)) {
        data.range.value = parseInt(RegExp.$1);
        if (["ft", "feet"].includes(RegExp.$2)) data.range.units = "ft";
        else if (["mi", "mile"].includes(RegExp.$2)) data.range.units = "mi";
      }
    }
  }

  _postProcessAction(src: string[]) {
    let data = this.data.data;

    // Saving throw
    {
      let stStr = src[15];
      if (!["", "NONE"].includes(stStr.toUpperCase())) {
        data.save.description = stStr;
      }
    }
    // Action type
    {
      if (data.spellEffect === "ray") {
        data.actionType = "rsak";
        data.ability.attack = "dex";
      }
      else if (data.subschool === "healing") {
        data.actionType = "heal";
      }
      else if (data.save.description.length) {
        data.actionType = "spellsave";
      }
      else if (data.spellEffect.length) {
        data.actionType = "other";
      }
    }
  }
}