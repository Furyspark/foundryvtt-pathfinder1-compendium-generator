export default class Importable {
  data: any

  toString(obj?: Object | Array<any>): string {
    if (obj === undefined) obj = this.data;
    else if( obj === null) return null;

    let str = (obj instanceof Array) ? "[" : "{";
    const entries = (obj instanceof Array) ? obj.map(v => {
      return [null, v];
    }) : Object.entries(obj);
    for (let a = 0; a < entries.length; a++) {
      const k = entries[a][0],
        v = entries[a][1];
      if (k != null) str += `"${k}":`;

      if (v instanceof Object || v instanceof Array) str += this.toString(v);
      else if (typeof v === "number" || typeof v === "boolean") str += v.toString();
      else if (typeof v === "string") str += `"${v}"`;
      else str += "null";

      if (a < entries.length - 1) str += ",";
    }
    return str + ((obj instanceof Array) ? "]" : "}");
  }
}