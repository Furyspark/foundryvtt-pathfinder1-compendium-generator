import { promises as fsPromises } from "fs";
const Papa = require("papaparse");
const fs = require("fs");
const path = require("path");
import Spell from "./importers/spell.js";

type ItemConstructor<T> = Function & {
  new (src: string[]): T
  prototype: T
}

export class Core {
  static spellNames: string[]

  static async start() {
    this.spellNames = [];

    let promises: Promise<any>[] = [];
    promises.push(this.parseItems("./db/spells.csv", "./spells*.db", Spell));
  }

  static async readDir(url: string): Promise<string[]> {
    let result = await fsPromises.readdir(url);

    return result.map(o => {
      return path.join(url, o);
    });
  }

  static async parseItems(csvUrl: string, outputUrl: string, type: ItemConstructor<Spell>) {
    const readStream = fs.createReadStream(csvUrl);
    const parseStream = Papa.parse(Papa.NODE_STREAM_INPUT);
    let writeStream = fs.createWriteStream(this.getFilename(outputUrl, 0));
    readStream.pipe(parseStream);

    let a = 0;
    let dbIndex = 0;
    parseStream.on("data", s => {
      if (a > 0) {
        const data = new type(s);
        writeStream.write(data.toString() + "\n");
      }
      ++a;
      if (a >= 5000) {
        a = 1;
        ++dbIndex;
        writeStream.end();
        writeStream = fs.createWriteStream(this.getFilename(outputUrl, dbIndex));
      }
    });

    parseStream.on("end", () => {
      writeStream.end();
    });
  }

  static getFilename(outputUrl: string, index: number): string {
    return outputUrl.replace("*", (index+1).toString());
  }
}