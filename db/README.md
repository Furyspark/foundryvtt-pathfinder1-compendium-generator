## Spells

The spells database is a CSV downloaded from [https://www.d20pfsrd.com/magic/tools/spells-db/](https://www.d20pfsrd.com/magic/tools/spells-db/)
Rename the file to `spells.csv`, and place it in the directory containing this readme.
