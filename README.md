# FoundryVTT Pathfinder 1e Compendium Generator

## Preparation

1. Download this repository.
2. Make sure you have TypeScript installed.
3. Enter the repository's directory.
4. Run `npm install`.
5. Follow the instructions located in `<repository directory>/db/README.md`.

## Usage

1. Run `npm run build`.
2. Run `npm run start`.
3. The generated compendium(s) should now be located in the repository's root directory. They have the .db extension.
